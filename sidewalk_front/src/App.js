import React, { Component } from 'react';
import SimpleMap from './components/MapComponent';
import './App.css';

class App extends Component {

  render() {
    return (
      <div>
        <SimpleMap/>
      </div>
    );
  }
}

export default App;