import React from 'react';
import '../App.css';

export const Login = (props) => {

    if (props.loggedIn === false) {
        return (
            <div className="loginButton">
                <span>
                    Login
                </span>
            </div>
        )
    } else {
        return(
            <div>
            </div>
        )
    }
}