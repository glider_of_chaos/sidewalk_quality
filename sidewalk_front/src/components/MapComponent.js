import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import { GetColor } from './ColorComponent';
import '../App.css';
import { Login } from './LoginComponent';
import { baseUrl } from '../shared/baseUrl';
import { googleApiKey } from '../shared/keys';


const AnyReactComponent = ({ text }) => <div>{text}</div>

const TEST_RESTRICTIONS = {
  north: 59.953,
  south: 59.947,
  west: 30.34,
  east: 30.354
};

class SimpleMap extends Component {

  handleApiLoaded (map, maps) {

    let geoJ = {};

    fetch(baseUrl + 'features')
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      geoJ = { "type": "FeatureCollection",
                "id": 0,
                "features": data};
      this.setState({
          currentFeatures: map.data.addGeoJson(geoJ),
          map: map
      });
    });

    let lineSymbol = {
      path: 'M 0,-1 0,1',
      strokeOpacity: 1,
      scale: 4
    };

    // Function to apply styles based on isSelected
    map.data.setStyle((feature) => {
      let color = 'gray';
      let zIndex = 3;
      let strokeWeight = 18;
      let strokeOpacity = 0;
      let icons = [{
          icon: lineSymbol,
          offset: '0',
          repeat: '20px'
        }];

      if (feature.getProperty('rating') !== undefined) {
        color = GetColor(parseFloat(feature.getProperty('rating')));
      }

      if (feature.getProperty('isSelected') === true || feature.getProperty('isSelected') === undefined) {
        zIndex = 2;
        strokeWeight = 3;
        strokeOpacity = 0.75;
        icons = []
      }

      return /** @type {google.maps.Data.StyleOptions} */ ({
        fillColor: color,
        strokeColor: color,
        strokeWeight: strokeWeight,
        zIndex: zIndex,
        strokeOpacity: strokeOpacity,
        icons: icons
      });
    });

  // Toggle colors on click
    map.data.addListener('click', (event) => {

      if (event.feature.getProperty('isSelected') !== true && event.feature.getProperty('isSelected') !== undefined) {
        event.feature.setProperty('isSelected', true);
      } else {
        event.feature.setProperty('isSelected', false);
      }

      let clickedId = event.feature.getProperty('@id');

      if (clickedId !== undefined && clickedId !== null) {
        let oldSelectedIds = [...this.state.selectedIds];
        let index = this.state.selectedIds.indexOf(clickedId);
        if (index === -1) {
          this.setState({
            selectedIds: [...oldSelectedIds, clickedId]
          });
        } else {
          oldSelectedIds.splice(index, 1);
          this.setState({
            selectedIds: oldSelectedIds
          });
        }
      }

      let baseRating = '';
      let baseSurface = '';
      if (this.state.selectedIds.length > 0) {
        let firstFeature = this.state.map.data.getFeatureById(this.state.selectedIds[0]);
        baseRating = firstFeature.getProperty('rating');
        baseSurface = firstFeature.getProperty('surface');
  
        for (let id of this.state.selectedIds) {
          let selectedFeature = this.state.map.data.getFeatureById(id);
          if (baseRating != selectedFeature.getProperty('rating')) {
            baseRating = '';
            break;
          }
        }
  
        for (let id of this.state.selectedIds) {
          let selectedFeature = this.state.map.data.getFeatureById(id);
          if (baseSurface !== selectedFeature.getProperty('surface')) {
            baseSurface = '';
            break;
          }
        }
      }
  
      if (this.state.ratingValue !== baseRating) {
        this.setState({
          ratingValue: baseRating
        });
      }
      if (this.state.surfaceValue !== baseSurface) {
        this.setState({
          surfaceValue: baseSurface,
        });
      }
    });

    // When the user hovers, tempt them to click by outlining the letters.
    // Call revertStyle() to remove all overrides. This will use the style rules
    // defined in the function passed to setStyle()
    map.data.addListener('mouseover', (event) => {
      map.data.revertStyle();
      map.data.overrideStyle(event.feature, {
        strokeWeight: 8,
        strokeOpacity: 1,
        zIndex: 4
      });
    });

    map.data.addListener('mouseout', (event) => {
      map.data.revertStyle();
    });
  };
 
  //class SimpleMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentFeatures: {},
      map: {},
      loggedIn: true,
      selectedIds: [],
      ratingValue: '',
      surfaceValue: '',
    };

    this.saveData = this.saveData.bind(this);
    this.updateFeature = this.updateFeature.bind(this);
  }

  static defaultProps = {
    center: {
      lat: 59.9495,
      lng: 30.349
    },
    zoom: 17
  };

  handleRatingChange(e) {
    this.setState({
      ratingValue: e.target.value
    });
  }

  handleSurfaceChange(e) {
    this.setState({
      surfaceValue: e.target.value
    });
  }

  saveData (e, updateFeature) {
    e.preventDefault();
    let selectedIds = [...this.state.selectedIds];
    
    for (let id of selectedIds) {
      let thisFeature = this.state.map.data.getFeatureById(id);
      let newProperties = {};
      thisFeature.forEachProperty(function(value,property) {
        if (property !== "isSelected") {
          newProperties[property] = value;
        }
      });

      if (this.state.selectedIds.length === 1 || 
          this.state.ratingValue !== '') {
        newProperties.rating = this.state.ratingValue;
        thisFeature.setProperty('rating', this.state.ratingValue);
      }
      if (this.state.selectedIds.length === 1 || 
          this.state.surfaceValue !== '') {
        newProperties.surface = this.state.surfaceValue;
        thisFeature.setProperty('surface', this.state.surfaceValue);
      }

      updateFeature(id, newProperties);
    }
  }

  updateFeature (id, properties) {
    fetch(baseUrl + 'features/' + id, {
      method: 'PATCH',
      body: JSON.stringify({
        "properties": properties
      }),
      headers: {
          'Content-Type': 'application/json'
      },
      credentials: 'same-origin'
    })
    .then(response => {
      if (response.ok) {
          return response;
      }
      else {
          var error = new Error('Error' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
      }
    },
    error => {
      var errmess = new Error(error.message);
      throw errmess;
    })
    .then(response => response.json())
    .catch(error => { console.log('update rating ', error.message);
        alert('Your update failed\nError: ' + error.message); });
  }

  render() {
    return (
      <div style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: googleApiKey }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
          options={{
            restriction: {
              latLngBounds: TEST_RESTRICTIONS,
              strictBounds: false
            }
          }}
          yesIWantToUseGoogleMapApiInternals
          onGoogleApiLoaded={({ map, maps }) => this.handleApiLoaded(map, maps)}
        >
          <AnyReactComponent
            lat={59.951}
            lng={30.35}
            text="My Marker"
          />
        </GoogleMapReact>
        <Login loggedIn={this.state.loggedIn}/>
        <div className="editForm">
          <form>
            <label htmlFor="IDs">IDs:{JSON.stringify(this.state.selectedIds)}</label><br/>
            <label htmlFor="rating">Rating:</label>
            <input type="number" min="0" max="5" step="0.1" id="rating" name="rating" value={this.state.ratingValue} onChange={(e)=>this.handleRatingChange(e)}></input>
            <label htmlFor="surface">Surface:</label>
            <input type="text" id="surface" name="surface" value={this.state.surfaceValue} onChange={(e)=>this.handleSurfaceChange(e)}></input>
            <button onClick={(e, updateFeature) => this.saveData(e, this.updateFeature)}>Save Data</button>
          </form>
        </div>
      </div>
    );
  }
}

export default SimpleMap;