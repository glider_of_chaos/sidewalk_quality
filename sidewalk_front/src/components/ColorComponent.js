
const RATING_COLORS = {
    0.0: "#880000",
    3.0: "#FFFF00",
    5.0: "#00CC00"
}

function ColorMixer(minColor, maxColor, rate) {
    let rMin = parseInt(minColor.slice(1,3), 16);
    let gMin = parseInt(minColor.slice(3,5), 16);
    let bMin = parseInt(minColor.slice(5,7), 16);
    let rMax = parseInt(maxColor.slice(1,3), 16);
    let gMax = parseInt(maxColor.slice(3,5), 16);
    let bMax = parseInt(maxColor.slice(5,7), 16);

    let rRes = rMin + Math.ceil((rMax - rMin)*rate);
    let gRes = gMin + Math.ceil((gMax - gMin)*rate);
    let bRes = bMin + Math.ceil((bMax - bMin)*rate);

    return ( "#" +  rRes.toString(16).padStart(2,0) +
                    gRes.toString(16).padStart(2,0) +
                    bRes.toString(16).padStart(2,0));
}

function GetColor(rating) {
    if (rating in RATING_COLORS) {
        return RATING_COLORS[rating];
    }
    else {
        if (rating < 3) {
            let rate = rating/3;
            return (ColorMixer(RATING_COLORS[0], RATING_COLORS[3], rate));
        }
        else {
            let rate = (rating - 3) / 5;
            return (ColorMixer(RATING_COLORS[3], RATING_COLORS[5], rate));
        }
    }

}

export { GetColor, ColorMixer };