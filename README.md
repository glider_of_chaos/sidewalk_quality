# README #

That's a capstone project for [React course from Coursera](https://www.coursera.org/learn/front-end-react).

The basic idea is to create a website that shows the quality of sidewalk pavement. The goals are:
* to display an easily readable map that shows color-coded sidewalks with colors corresponding to the quality of the pavement (dark red for example being really bad while green means nice and smooth)
* to allow user to rate the quality of the pavement on a 1 to 5 scale and displaying those results.

It's a partial remake of [old github project](https://github.com/nikolaybolonin/Quality-of-Roads) I attempted with my friend.

# GENERAL INFO #

* Name: Sidewalk quality project
* Version: 0.01

# SET-UP INSTRUCTION FOR FAKE BACKEND

Build with
..../j_server$ docker build -t djserver .

Run with
$ docker run -d --rm -it --name jsonserver-container -p 8080:8080 djserver

# SET-UP INSTRUCTIONS FOR FRONTEND PART

Build with
.../sidewal_front$ docker build -t sidewalk:dev .

Run with
$ docker run -v ${PWD}:/app -v /app/node_modules -p 3001:3000 --rm sidewalk:dev